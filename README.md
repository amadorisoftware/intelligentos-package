# IntelligentOS-Package

All the package installed on **IntelligentOS v0.1-Beta** are; **LibreWolf** v87.0-1 Flatpak), **Virtualbox** v6.16 (APT),
**VS** **Codium** v1.55.1 (DEB), **SimpleScreenRecorder** v0.3.11 (APT), **BitWarden** v1.25.1 (Flatpak), **TOR Browser** v0.3 (Flatpak).
Thanks for seeing this file and thanks for using our Beta Distro. Enjoy It!
